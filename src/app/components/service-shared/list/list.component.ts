import { Component, OnInit } from '@angular/core';

import { HeroService } from '../../../services/hero.service';
import { IHeroCharacter } from '../../../interfaces/hero.interface';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  heroList: IHeroCharacter[];

  constructor(private heroService: HeroService) {
    this.heroList = this.heroService.getHeroList();
  }

  ngOnInit() {}
}
